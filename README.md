# 126学习文档共享

```
青，取之于蓝，而青于蓝；冰，水为之，而寒于水。
木直中绳，輮以为轮，其曲中规。虽有槁暴，不复挺者，輮使之然也。
故木受绳则直，金就砺则利，君子博学而日参省乎己，则知明而行无过矣。
```

## 李的文档

[走你](https://gitee.com/j6l/md-file/tree/md-jie/) 

## 闻的文档

[走你](https://gitee.com/j6l/md-file/tree/md-jun/) 

## 6的文档

[走你](https://gitee.com/j6l/md-file/tree/md-6/) 
